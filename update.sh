#!/bin/bash
echo "Starting update!" > updater.log
sleep 5
cd /home/langerhans/shibebot
killall java >> updater.log
git pull >> updater.log
./gradlew clean build >> updater.log
nohup java -jar build/libs/shibebot.jar &
disown

