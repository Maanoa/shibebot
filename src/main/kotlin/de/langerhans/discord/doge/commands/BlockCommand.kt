package de.langerhans.discord.doge.commands

import de.langerhans.discord.doge.blockcypher.BlockcypherService
import sx.blah.discord.handle.impl.events.MessageReceivedEvent
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * Created by maxke on 01.04.2016.
 * Check a block
 */

class BlockCommand : Command {

    private val service = BlockcypherService.create()

    override fun trigger(): String {
        return "block"
    }

    override fun type(): CommandType {
        return CommandType.TRIGGER
    }

    override fun name(): String {
        return BlockCommand::class.java.simpleName
    }

    override fun handle(e: MessageReceivedEvent): String {
        val parts = e.message.content.split(" ")
        if (parts.size < 2) {
            return "Not enough arguments. Try `!block 60323982f9c5ff1b5a954eac9dc1269352835f47c2c5222691d80f0d50dcf053`"
        }

        val hash = parts[1]

        val resp = service.getBlockInfo(hash).execute()

        if (resp.isSuccessful) {
            val block = resp.body()
            val total = BigDecimal(block.total/100000000).setScale(8, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString()
            val fees = BigDecimal(block.fees/100000000).setScale(8, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString()
            val chain = block.chain.takeWhile { c -> c != '.' }.toUpperCase()

            val info = "Block **${block.height}** has **${block.depth}** confirmations. It includes **${block.n_tx}** transactions which make it **${block.size}** byte large. In total **$total** $chain were sent and **$fees** $chain were paid in fees."
            return info
        } else {
            return "Failed to get block from API. Sorry!"
        }
    }
}