package de.langerhans.discord.doge.commands

import de.langerhans.discord.doge.authorIsBotAdmin
import sx.blah.discord.api.IDiscordClient
import sx.blah.discord.handle.impl.events.MessageReceivedEvent
import uy.kohesive.injekt.injectLazy
import java.io.File

/**
 * Created by maxke on 31.03.2016.
 * The update command
 */

class UpdateCommand : Command {

    val client: IDiscordClient by injectLazy()

    override fun trigger(): String {
        return "update"
    }

    override fun type(): CommandType {
        return CommandType.TRIGGER
    }

    override fun name(): String {
        return UpdateCommand::class.java.simpleName
    }

    override fun handle(e: MessageReceivedEvent): String? {
        if (!e.authorIsBotAdmin()) {
            return null
        }

        val dir = System.getProperty("user.dir") + File.separator
        val script = "${dir}update.sh > ${dir}updater.log 2>&1"
        Runtime.getRuntime().exec(script)

        e.message.channel.sendMessage("__***Updating myself from git... Be right back...***__")
        client.logout()

        return null
    }

}