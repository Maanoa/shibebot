package de.langerhans.discord.doge.commands

import com.j256.ormlite.dao.Dao
import de.langerhans.discord.doge.db.dao.User
import sx.blah.discord.handle.impl.events.MessageReceivedEvent
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.time.Duration

/**
 * Created by maxke on 09.04.2016.
 * Check when a user was last seen
 */

class SeenCommand: Command {
    private val userDao: Dao<User, String> = Injekt.get()

    override fun handle(e: MessageReceivedEvent): String? {
        val mentions = e.message.mentions
        if (mentions.size > 1) {
            return "Please only request this for a single user!"
        }
        val target = mentions.first()
        if (target.isBot) {
            return "This command is not available for bots."
        }

        val user = userDao.queryForId(target.id) ?: return "I have not seen ${mentions.first().name}."
        if (user.lastSeenType == SeenWatcher.SeenType.UNKNOWN) {
            return "I have not seen ${target.name}."
        }

        var guild = "on this server"
        if (user.lastSeenGuildId != e.message.guild.id) {
            guild = "on another server"
        }

        val currentTime = System.currentTimeMillis()
        val ago = Duration.ofMillis(currentTime - user.lastSeenTime)

        var msg: String
        when (user.lastSeenType) {
            SeenWatcher.SeenType.MESSAGE -> msg = "${target.name}#${target.discriminator} was last seen sending a message **${formatAgo(ago)}** ago $guild."
            SeenWatcher.SeenType.LOGIN -> msg = "${target.name}#${target.discriminator} was last logging in **${formatAgo(ago)}** ago."
            SeenWatcher.SeenType.LOGOUT -> msg = "${target.name}#${target.discriminator} was last logging out **${formatAgo(ago)}** ago."
            SeenWatcher.SeenType.IDLE -> msg = "${target.name}#${target.discriminator} was last becoming idle **${formatAgo(ago)}** ago."
            SeenWatcher.SeenType.ACTIVE -> msg = "${target.name}#${target.discriminator} was last becoming active again **${formatAgo(ago)}** ago."
            else -> msg = "Unknown error occurred!"
        }

        return msg
    }

    private fun formatAgo(d: Duration): String {
        val sec = (d.seconds % 60).toInt()
        val min = (d.toMinutes() % 60).toInt()
        val hours = (d.toHours() % 24).toInt()
        val days = (d.seconds / 86400).toInt()

        var s = ""
        if(days > 0) {
            if (days == 1) {
                s += "1 day, "
            } else {
                s += "$days days, "
            }
        }
        if(hours > 0) {
            if (hours == 1) {
                s += "1 hour, "
            } else {
                s += "$hours hours, "
            }
        }
        if(min > 0) {
            if (min == 1) {
                s += "1 minute, "
            } else {
                s += "$min minutes, "
            }
        }
        if(sec > 0) {
            if (sec == 1) {
                s += "1 second, "
            } else {
                s += "$sec seconds, "
            }
        }

        if(s.takeLast(2) == ", ") {
            s = s.dropLast(2)
        }

        return s
    }

    override fun trigger(): String {
        return "seen"
    }

    override fun type(): CommandType {
        return CommandType.TRIGGER
    }

    override fun name(): String {
        return SeenCommand::class.java.simpleName
    }

}