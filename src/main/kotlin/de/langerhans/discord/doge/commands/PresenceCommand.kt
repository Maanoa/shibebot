package de.langerhans.discord.doge.commands

import de.langerhans.discord.doge.authorIsBotAdmin
import sx.blah.discord.handle.impl.events.MessageReceivedEvent
import java.util.*

/**
 * Created by maxke on 31.03.2016.
 * The help command
 */

class PresenceCommand : Command {

    override fun trigger(): String {
        return "presence"
    }

    override fun type(): CommandType {
        return CommandType.TRIGGER
    }

    override fun name(): String {
        return PresenceCommand::class.java.simpleName
    }

    override fun handle(e: MessageReceivedEvent): String? {
        if (!e.authorIsBotAdmin()) {
            return null
        }

        val parts = e.message.content.split(" ")
        val idle = parts[1].toBoolean()
        val game = parts.drop(2).joinToString(" ")

        e.client.updatePresence(idle, Optional.of(game))
        return null
    }

}