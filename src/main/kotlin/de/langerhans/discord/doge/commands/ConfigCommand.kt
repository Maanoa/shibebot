package de.langerhans.discord.doge.commands

import de.langerhans.discord.doge.Config
import de.langerhans.discord.doge.authorCanManageServer
import de.langerhans.discord.doge.web.WebAccessStorage
import sx.blah.discord.handle.impl.events.MessageReceivedEvent
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

/**
 * Created by maxke on 31.03.2016.
 * The help command
 */

class ConfigCommand : Command {

    override fun trigger(): String {
        return "config"
    }

    override fun type(): CommandType {
        return CommandType.TRIGGER
    }

    override fun name(): String {
        return ConfigCommand::class.java.simpleName
    }

    override fun handle(e: MessageReceivedEvent): String? {
        if (!e.authorCanManageServer()) {
            return null
        }

        val config = Injekt.get<Config>()
        var url = config.config.baseurl
        if (!url.endsWith("/")) url += "/"

        val access = Injekt.get<WebAccessStorage>()
        e.client.getOrCreatePMChannel(e.message.author).sendMessage("${url}gateway?t=${access.generateToken(e.message.guild.id)}")
        return null
    }

}