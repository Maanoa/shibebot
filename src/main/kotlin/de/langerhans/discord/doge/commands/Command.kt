package de.langerhans.discord.doge.commands

import sx.blah.discord.handle.impl.events.MessageReceivedEvent

/**
 * Created by maxke on 31.03.2016.
 * Base interface for commands
 */
interface Command {
    fun handle(e: MessageReceivedEvent): String?
    fun trigger(): String
    fun type(): CommandType
    fun name(): String
}

enum class CommandType {
    TRIGGER,
    FULLTEXT,
    WATCHER
}