package de.langerhans.discord.doge.commands

import de.langerhans.discord.doge.cryptonator.CryptonatorService
import sx.blah.discord.handle.impl.events.MessageReceivedEvent

/**
 * Created by maxke on 01.04.2016.
 * Check doge price
 */

class DogeCommand : Command {

    private val service = CryptonatorService.create()
    private val markets = listOf("Bittrex", "Bter", "C-CEX", "Poloniex", "Vircurex")

    override fun trigger(): String {
        return "doge"
    }

    override fun type(): CommandType {
        return CommandType.TRIGGER
    }

    override fun name(): String {
        return DogeCommand::class.java.simpleName
    }

    override fun handle(e: MessageReceivedEvent): String {
        val resp = service.fullTicker("doge", "btc").execute()

        if (resp.isSuccessful) {
            val s = StringBuilder()
            resp.body().ticker.markets.forEach { market -> run{
                if (market.market in markets) {
                    s.append("*${market.market}:* **${market.price}** ")
                }
            }}
            return s.toString().trim()
        } else {
            return "Failed to get prices from API. Sorry!"
        }
    }
}