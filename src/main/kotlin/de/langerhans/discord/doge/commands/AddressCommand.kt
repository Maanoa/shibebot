package de.langerhans.discord.doge.commands

import de.langerhans.discord.doge.blockcypher.BlockcypherService
import sx.blah.discord.handle.impl.events.MessageReceivedEvent
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * Created by maxke on 01.04.2016.
 * Check an address
 */

class AddressCommand : Command {

    private val service = BlockcypherService.create()

    override fun trigger(): String {
        return "address"
    }

    override fun type(): CommandType {
        return CommandType.TRIGGER
    }

    override fun name(): String {
        return AddressCommand::class.java.simpleName
    }

    override fun handle(e: MessageReceivedEvent): String {
        val parts = e.message.content.split(" ")
        if (parts.size < 2) {
            return "Not enough arguments. Try `!address 9x9zSN9vx3Kf9B4ofwzEfWgiqxwBieCNEb`"
        }

        val address = parts[1]

        val resp = service.getAddressInfo(address).execute()

        if (resp.isSuccessful) {
            val addr = resp.body()
            val total = BigDecimal(addr.final_balance/100000000).setScale(8, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString()
            val sent = BigDecimal(addr.total_sent/100000000).setScale(8, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString()
            val recvd = BigDecimal(addr.total_received/100000000).setScale(8, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString()
            val unconf = BigDecimal(addr.unconfirmed_balance/100000000).setScale(8, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString()
            // TODO get from url: val chain = addr.chain.takeWhile { c -> c != '.' }.toUpperCase()
            val chain = "DOGE"

            var unconfPart = ""
            if (addr.unconfirmed_balance != 0L) {
                unconfPart = " of which are **$unconf** $chain are still unconfirmed"
            }
            val info = "This address has a total balance of **$total** $chain$unconfPart. So far it has seen **${addr.final_n_tx}** transactions, sent **$sent** $chain and received **$recvd** $chain."
            return info
        } else {
            return "Failed to get address info from API. Sorry!"
        }
    }
}