package de.langerhans.discord.doge.commands

import sx.blah.discord.handle.impl.events.MessageReceivedEvent

/**
 * Created by maxke on 31.03.2016.
 * The help command
 */

class HelpCommand : Command {

    override fun trigger(): String {
        return "help"
    }

    override fun type(): CommandType {
        return CommandType.TRIGGER
    }

    override fun name(): String {
        return HelpCommand::class.java.simpleName
    }

    override fun handle(e: MessageReceivedEvent): String? {
        if (!e.message.content.trim().endsWith(trigger())) {
            // We currently don't support command wise help...
            return null
        }
        return "Hi! I support the following commands: `!help`, `!glitter`, `!doge`, `!convert`, `!gif`, `!seen`, `!block`, `!address`, `!tx`, `!chain`"
    }

}