package de.langerhans.discord.doge.commands

import sx.blah.discord.handle.impl.events.MessageReceivedEvent

/**
 * Created by maxke on 31.03.2016.
 * The glitter command
 */

class GlitterCommand : Command {
    override fun trigger(): String {
        return "glitter"
    }

    override fun type(): CommandType {
        return CommandType.TRIGGER
    }

    override fun name(): String {
        return GlitterCommand::class.java.simpleName
    }

    override fun handle(e: MessageReceivedEvent): String {
        return "☆.。.:*・°☆.。.:*・°☆.。.:*・°☆.。.:*・°☆"
    }

}