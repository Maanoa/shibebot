package de.langerhans.discord.doge.commands

import de.langerhans.discord.doge.blockcypher.BlockcypherService
import sx.blah.discord.handle.impl.events.MessageReceivedEvent
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * Created by maxke on 01.04.2016.
 * Check a tx
 */

class TxCommand : Command {

    private val service = BlockcypherService.create()

    override fun trigger(): String {
        return "tx"
    }

    override fun type(): CommandType {
        return CommandType.TRIGGER
    }

    override fun name(): String {
        return TxCommand::class.java.simpleName
    }

    override fun handle(e: MessageReceivedEvent): String {
        val parts = e.message.content.split(" ")
        if (parts.size < 2) {
            return "Not enough arguments. Try `!tx 6f47f0b2e1ec762698a9b62fa23b98881b03d052c9d8cb1d16bb0b04eb3b7c5b`"
        }

        val hash = parts[1]

        val resp = service.getTxInfo(hash).execute()

        if (resp.isSuccessful) {
            val tx = resp.body()
            val total = BigDecimal(tx.total/100000000).setScale(8, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString()
            val fees = BigDecimal(tx.fees/100000000).setScale(8, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString()
            // TODO get from url: val chain = tx.chain.takeWhile { c -> c != '.' }.toUpperCase()
            val chain = "DOGE"

            val info = "This transaction has **${tx.confirmations}** confirmations and sends a total of **$total** $chain. For this it paid **$fees** $chain in fees and used **${tx.vin_sz}** inputs and **${tx.vout_sz}** outputs, making it **${tx.size}** byte large."
            return info
        } else {
            return "Failed to get transaction from API. Sorry!"
        }
    }
}