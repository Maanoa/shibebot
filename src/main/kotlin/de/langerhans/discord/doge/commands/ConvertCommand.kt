package de.langerhans.discord.doge.commands

import de.langerhans.discord.doge.cryptonator.CryptonatorService
import sx.blah.discord.handle.impl.events.MessageReceivedEvent
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * Created by maxke on 01.04.2016.
 * Convert currencies
 */

class ConvertCommand : Command {

    private val service = CryptonatorService.create()

    override fun trigger(): String {
        return "convert"
    }

    override fun type(): CommandType {
        return CommandType.TRIGGER
    }

    override fun name(): String {
        return ConvertCommand::class.java.simpleName
    }

    override fun handle(e: MessageReceivedEvent): String {
        val parts = e.message.content.split(" ")
        if (parts.size < 4) {
            return "Not enough arguments. Try `!convert 1000 doge btc`"
        }

        val amount: BigDecimal
        try {
            amount = BigDecimal(parts[1]).setScale(8, RoundingMode.HALF_DOWN).stripTrailingZeros()
        } catch (e: NumberFormatException) {
            return "First parameter has to be a valid number! Try `!convert 1000 doge btc`"
        }

        val resp = service.singleTicker(parts[2], parts[3]).execute()

        if (resp.isSuccessful && resp.body().error == "") {
            val price = BigDecimal(resp.body().ticker.price.toDouble())
            val result = price.multiply(amount).setScale(8, RoundingMode.HALF_DOWN).stripTrailingZeros()

            return "**${amount.toPlainString()}** ${resp.body().ticker.base} = **${result.toPlainString()}** ${resp.body().ticker.target} (*Cryptonator*)"
        } else {
            return "Failed to get prices from API. Sorry!"
        }
    }
}