package de.langerhans.discord.doge.commands

import de.langerhans.discord.doge.authorIsBotAdmin
import sx.blah.discord.handle.impl.events.MessageReceivedEvent
import sx.blah.discord.util.Image
import java.util.regex.Pattern

/**
 * Created by maxke on 31.03.2016.
 * The avatar command
 */

class AvatarCommand : Command {
    override fun trigger(): String {
        return "avatar"
    }

    override fun type(): CommandType {
        return CommandType.TRIGGER
    }

    override fun name(): String {
        return AvatarCommand::class.java.simpleName
    }

    override fun handle(e: MessageReceivedEvent): String? {
        if (!e.authorIsBotAdmin()) {
            return null
        }

        val pattern = Pattern.compile("i\\.imgur\\.com\\/\\w+\\.(jpg|png)")
        val matcher = pattern.matcher(e.message.content)
        if (matcher.find()) {
            val imgUrl = "http://${matcher.group()}"
            e.client.changeAvatar(Image.forUrl(matcher.group(1), imgUrl))
            return "Done!"
        } else {
            return "Only imgur jpg or png links supported!"
        }
    }

}