package de.langerhans.discord.doge.commands

import com.j256.ormlite.dao.Dao
import de.langerhans.discord.doge.db.dao.GifConfig
import de.langerhans.discord.doge.giphy.GiphyService
import sx.blah.discord.handle.impl.events.MessageReceivedEvent
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.util.*

/**
 * Created by maxke on 01.04.2016.
 * Convert currencies
 */

class GifCommand : Command {

    private val service = GiphyService.create()
    private val configDao = Injekt.get<Dao<GifConfig, String>>()

    override fun trigger(): String {
        return "gif"
    }

    override fun type(): CommandType {
        return CommandType.TRIGGER
    }

    override fun name(): String {
        return GifCommand::class.java.simpleName
    }

    override fun handle(e: MessageReceivedEvent): String {
        val parts = e.message.content.split(" ")
        if (parts.size < 2) {
            return "Not enough arguments. Try `!gif doge`"
        }

        var rating = configDao.queryForId(e.message.guild.id)?.rating
        if (rating == null) {
            rating = GifConfig().rating
        }
        val resp = service.searchGif(parts.drop(1).joinToString(" "), rating).execute()

        if (resp.isSuccessful && resp.body().meta.status == 200) {
            val gifs = resp.body().data
            val max = gifs.size
            val n = Random().nextInt((max - 1) + 1)
            return gifs[n].images.fixed_height.url
        } else {
            return "Failed to get gif. Sorry!"
        }
    }
}