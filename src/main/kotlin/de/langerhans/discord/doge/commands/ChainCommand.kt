package de.langerhans.discord.doge.commands

import de.langerhans.discord.doge.blockcypher.BlockcypherService
import sx.blah.discord.handle.impl.events.MessageReceivedEvent
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * Created by maxke on 01.04.2016.
 * Check the chain
 */

class ChainCommand : Command {

    private val service = BlockcypherService.create()

    override fun trigger(): String {
        return "chain"
    }

    override fun type(): CommandType {
        return CommandType.TRIGGER
    }

    override fun name(): String {
        return ChainCommand::class.java.simpleName
    }

    override fun handle(e: MessageReceivedEvent): String {
        val resp = service.getChainInfo().execute()

        if (resp.isSuccessful) {
            val chain = resp.body()
            val chainNet = chain.name.split(".")

            var forkPart = ""
            if (chain.last_fork_height != 0) {
                forkPart = " The chain has last forked at height **${chain.last_fork_height}**."
            }

            val info = "The **${chainNet[0]} ${chainNet[1]}** chain has **${chain.height}** blocks. The latest block is **${chain.hash}** and currently **${chain.unconfirmed_count}** transaction are waiting to be confirmed.$forkPart"
            return info
        } else {
            return "Failed to get block from API. Sorry!"
        }
    }
}