package de.langerhans.discord.doge.commands

import com.j256.ormlite.dao.Dao
import de.langerhans.discord.doge.db.dao.User
import org.slf4j.Logger
import sx.blah.discord.handle.impl.events.MessageReceivedEvent
import sx.blah.discord.handle.impl.events.PresenceUpdateEvent
import sx.blah.discord.handle.obj.Presences
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import uy.kohesive.injekt.injectLogger

/**
 * Created by maxke on 08.04.2016.
 * Logs when and where users where seen the last time
 */

class SeenWatcher: Command {
    private val log: Logger by injectLogger()
    private val userDao: Dao<User, String> = Injekt.get()

    enum class SeenType {
        UNKNOWN,
        MESSAGE,
        LOGIN,
        LOGOUT,
        IDLE,
        ACTIVE
    }

    override fun handle(e: MessageReceivedEvent): String? {
        val user = e.message.author.id
        val guild = e.message.guild.id
        val time = System.currentTimeMillis()
        val type = SeenType.MESSAGE

        saveEvent(user, guild, time, type)

        return null
    }

    fun handlePresence(e: PresenceUpdateEvent) {
        val user = e.user.id
        val guild = e.guild.id
        val time = System.currentTimeMillis()

        var type: SeenType
        when (e.newPresence) {
            Presences.ONLINE -> if(e.oldPresence == Presences.IDLE) type = SeenType.ACTIVE else type = SeenType.LOGIN
            Presences.IDLE -> type = SeenType.IDLE
            Presences.OFFLINE -> type = SeenType.LOGOUT
            else -> type = SeenType.UNKNOWN
        }

        saveEvent(user, guild, time, type)
    }

    private fun saveEvent(user: String, guild: String, time: Long, type: SeenType) {
        var pUser = userDao.queryForId(user)
        if (pUser == null) {
            pUser = User(user)
        }
        pUser.lastSeenTime = time
        pUser.lastSeenType = type
        pUser.lastSeenGuildId = guild

        userDao.createOrUpdate(pUser)
    }

    override fun trigger(): String {
        return ""
    }

    override fun type(): CommandType {
        return CommandType.WATCHER
    }

    override fun name(): String {
        return SeenWatcher::class.java.simpleName
    }
}