package de.langerhans.discord.doge.web

import ro.pippo.core.Application
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.addSingleton
import uy.kohesive.injekt.api.get

/**
 * Created by maxke on 19.04.2016.
 * Main Entry for the web config
 */

class BotConfigApplication() : Application() {
    override fun onInit() {
        addWebjarsResourceRoute()
        WebInjekt().registerWith(Injekt)

        // Index
        GET("/", {routeContext -> Injekt.get<IndexController>().handle(routeContext)})

        // Gateway to config
        GET("/gateway", {routeContext -> Injekt.get<GatewayController>().handle(routeContext)})

        // Config page
        GET("/config", {routeContext -> Injekt.get<ConfigController>().handleGet(routeContext)})
        POST("/config", {routeContext -> Injekt.get<ConfigController>().handlePost(routeContext)})
    }


    class WebInjekt: InjektModule {
        override fun InjektRegistrar.registerInjectables() {
            addSingleton(IndexController())
            addSingleton(GatewayController())
            addSingleton(ConfigController())
        }
    }
}