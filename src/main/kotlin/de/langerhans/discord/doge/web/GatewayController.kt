package de.langerhans.discord.doge.web

import org.slf4j.Logger
import ro.pippo.core.route.RouteContext
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import uy.kohesive.injekt.injectLogger
import java.util.concurrent.TimeUnit


/**
 * Created by maxke on 14.04.2016.
 * Authorizes user and sets session
 */

class GatewayController {
    private val logger: Logger by injectLogger()

    fun handle(routeContext: RouteContext) {
        var guildId: String?
        if (routeContext.hasSession()) {
            guildId = routeContext.getSession("guildId")
        } else {
            val token = routeContext.request.queryParameters["t"].toString()
            guildId = Injekt.get<WebAccessStorage>().checkToken(token)
        }

        if (guildId == null) {
            logger.error("Access violation by ${routeContext.request.clientIp} | ${routeContext.request.userAgent}. Invalid token!")
            routeContext.redirect("/error")
            return
        } else {
            routeContext.setSession("guildId", guildId)
            routeContext.session.httpSession.maxInactiveInterval = TimeUnit.MINUTES.toSeconds(5).toInt()
            routeContext.redirect("/config")
        }
    }
}