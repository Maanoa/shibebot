package de.langerhans.discord.doge.web

import com.j256.ormlite.dao.Dao
import de.langerhans.discord.doge.db.dao.GifConfig
import org.slf4j.Logger
import ro.pippo.core.route.RouteContext
import sx.blah.discord.api.IDiscordClient
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import uy.kohesive.injekt.injectLogger


/**
 * Created by maxke on 14.04.2016.
 * Handles the config page
 */
class ConfigController {

    private val configDao = Injekt.get<Dao<GifConfig, String>>()
    private val logger: Logger by injectLogger()
    val allowed = listOf("y", "g", "pg", "pg-13", "r")

    fun handleGet(routeContext: RouteContext) {
        if (!routeContext.hasSession()) {
            logger.info("Invalid session from ${routeContext.request.clientIp} | ${routeContext.request.userAgent}.")
            routeContext.redirect("/error")
            return
        }

        val guildId: String = routeContext.getSession("guildId")
        val current = configDao.queryForId(guildId).rating // TODO: Fix!

        val guild = Injekt.get<IDiscordClient>().getGuildByID(guildId)

        val model = mapOf(
                "ratings" to allowed,
                "currentRating" to current,
                "guildName" to guild.name,
                "guildId" to guild.id,
                "guildIcon" to guild.iconURL,
                "guildChannelCnt" to guild.channels.size,
                "guildUserCnt" to guild.users.size)

        routeContext.setLocals(model)
        routeContext.render("config")
    }

    fun handlePost(routeContext: RouteContext) {
        if (!routeContext.hasSession()) {
            logger.info("Invalid session from ${routeContext.request.clientIp} | ${routeContext.request.userAgent}.")
            routeContext.redirect("/error")
            return
        }

        val newRating = routeContext.request.queryParameters["rating"].toString()
        val guildId: String = routeContext.getSession("guildId")
        if (newRating in allowed) {
            var config = configDao.queryForId(guildId)

            if (config == null) {
                config = GifConfig(guildId)
            }

            config.rating = newRating

            configDao.update(config)
        }

        // Now render config page again
        handleGet(routeContext)
    }
}