package de.langerhans.discord.doge.web

import ro.pippo.core.route.RouteContext

/**
 * Created by maxke on 13.04.2016.
 * Homepage controller
 */

class IndexController {
    fun handle(routeContext: RouteContext) {
        routeContext.render("index")
    }
}