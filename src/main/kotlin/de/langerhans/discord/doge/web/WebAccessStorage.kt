package de.langerhans.discord.doge.web

import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by maxke on 20.04.2016.
 * Handles generation and check of access tokens
 */

class WebAccessStorage {
    private val tokenValidity = TimeUnit.SECONDS.toMillis(60)
    private val tokens = HashMap<String, Pair<Long, String>>()

    fun generateToken(guildId: String): String {
        val token = UUID.randomUUID().toString()
        tokens.put(token, Pair(System.currentTimeMillis().plus(tokenValidity), guildId))
        return token
    }

    fun checkToken(token: String): String? {
        if (tokens.containsKey(token)) {
            val saved = tokens[token]
            if (saved != null && saved.first > System.currentTimeMillis()) {
                return saved.second
            }
        }
        return null
    }
}