package de.langerhans.discord.doge

import com.j256.ormlite.dao.Dao
import de.langerhans.discord.doge.commands.*
import de.langerhans.discord.doge.db.dao.Guild
import de.langerhans.discord.doge.reddit.RedditWatcher
import org.slf4j.Logger
import sx.blah.discord.api.EventSubscriber
import sx.blah.discord.handle.impl.events.*
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import uy.kohesive.injekt.injectLazy
import uy.kohesive.injekt.injectLogger
import java.util.*
import kotlin.concurrent.thread

/**
 * Created by maxke on 31.03.2016.
 * Dispatches Events around
 */

class EventHandler {

    private val commands = HashSet<Command>()
    private val log: Logger by injectLogger()
    private val config: Config by injectLazy()
    private val triggerChar = config.config.trigger

    init {
        // Trigger commands
        commands.add(HelpCommand())
        commands.add(GlitterCommand())
        commands.add(DogeCommand())
        commands.add(AvatarCommand())
        commands.add(ConvertCommand())
        commands.add(GifCommand())
        commands.add(UpdateCommand())
        commands.add(SeenCommand())
        commands.add(PresenceCommand())
        commands.add(ConfigCommand())
        commands.add(BlockCommand())
        commands.add(TxCommand())
        commands.add(AddressCommand())
        commands.add(ChainCommand())

        // Watcher "commands"
        commands.add(SeenWatcher())
    }

    @EventSubscriber fun onReadyEvent(e: ReadyEvent) {
        log.info("Bot is ready")
        RedditWatcher().start()
    }

    @EventSubscriber fun onDiscordDisconnectedEvent(e: DiscordDisconnectedEvent){
        log.info("Got disconnected with reason ${e.reason.name}")
        e.client.logout() // Try to logout since sometimes the WS closes unexpectedly :|
        thread { e.client.login() }
    }

    @EventSubscriber fun onMessageReceivedEvent(e: MessageReceivedEvent) {
        val text = e.message.content
        if (text.startsWith(triggerChar)) {
            val end = if (text.contains(" ")) text.indexOf(" ")-1 else text.length-1
            val trigger = text.slice(1..end)
            val result = commands.filter { it.trigger() == trigger }.firstOrNull()?.handle(e)

            if (result != null) {
                log.info("Received command: $text from user ${e.message.author.name}")
                e.reply(result)
            }
        }

        // After it is handled, the passive watchers can know about it
        commands.filter { it.type() == CommandType.WATCHER }.forEach { it.handle(e) }
    }

    @EventSubscriber fun onPresenceUpdateEvenet(e: PresenceUpdateEvent) {
        // Special event for SeenWatcher
        val cmd = commands.filter { it.name() == SeenWatcher::class.java.simpleName }.first() as SeenWatcher
        cmd.handlePresence(e)
    }

    @EventSubscriber fun onGuildCreatedEvent(e: GuildCreateEvent) {
        log.info("Joined guild ${e.guild.name}")
        val guildDao = Injekt.get<Dao<Guild, String>>()
        guildDao.createIfNotExists(Guild(e.guild.id))
    }
}