package de.langerhans.discord.doge.blockcypher

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by maxke on 31.03.2016.
 * API service to get data from chain.so
 */

interface BlockcypherService {

    @GET("{coin}/{net}/addrs/{addr}")
    fun getAddressInfo(@Path("addr") addr: String, @Path("coin") coin: String = "doge", @Path("net") network: String = "main"): Call<Address>

    @GET("{coin}/{net}/txs/{txid}")
    fun getTxInfo(@Path("txid") txid: String, @Path("coin") coin: String = "doge", @Path("net") network: String = "main"): Call<Tx>

    @GET("{coin}/{net}/blocks/{hash}")
    fun getBlockInfo(@Path("hash") hash: String, @Path("coin") coin: String = "doge", @Path("net") network: String = "main"): Call<Block>

    @GET("{coin}/{net}")
    fun getChainInfo(@Path("coin") coin: String = "doge", @Path("net") network: String = "main"): Call<Chain>

    companion object {
        fun create(): BlockcypherService {
            val retrofit = Retrofit.Builder()
                    .baseUrl("https://api.blockcypher.com/v1/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit.create(BlockcypherService::class.java)
        }
    }
}