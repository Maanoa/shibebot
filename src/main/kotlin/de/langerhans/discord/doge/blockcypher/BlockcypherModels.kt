package de.langerhans.discord.doge.blockcypher

/**
 * Created by maxke on 21.04.2016.
 * Model classes for chain.so
 */

data class Block (
        val hash: String,
        val height: Int,
        val depth: Int,
        val chain: String,
        val total: Long,
        val fees: Long,
        val size: Int,
        val ver: Int,
        val time: String,
        val received_time: String,
        val relayed_by: String,
        val bits: Int,
        val nonce: Int,
        val n_tx: Int,
        val prev_block: String,
        val prev_block_url: String,
        val tx_url: String,
        val mrkl_root: String,
        val txids: List<String>,
        val next_txids: String
)

data class Tx (
        val block_height: Int,
        val hash: String,
        val addresses: List<String>,
        val total: Long,
        val fees: Long,
        val size: Int,
        val preference: String,
        val relayed_by: String,
        val received: String,
        val ver: Int,
        val lock_String: Int,
        val double_spend: Boolean,
        val vin_sz: Int,
        val vout_sz: Int,
        val confirmations: Int,
        //val inputs: List<TxInput>,
        //val outputs: List<TxOutput>,
        val confidence: Float,
        val confirmed: String,
        val receive_count: Int,
        val change_address: String,
        val block_hash: String,
        val double_of: String,
        val data_protocol: String,
        val hex: String,
        val next_inputs: String,
        val next_outputs: String
)

data class Address (
        val address: String,
        //val wallet: Wallet,
        //val hd_wallet: HdWallet,
        val total_received: Long,
        val total_sent: Long,
        val balance: Long,
        val unconfirmed_balance: Long,
        val final_balance: Long,
        val n_tx: Int,
        val unconfirmed_n_tx: Int,
        val final_n_tx: Int,
        val tx_url: String,
        val txs: List<Tx>,
        //val txrefs: List<TxRef>,
        //val unconfirmed_txrefs: List<TxRef>,
        val hasMore: Boolean
)

data class Chain (
        val name: String,
        val height: Int,
        val hash: String,
        val time: String,
        val latest_url: String,
        val previous_hash: String,
        val previous_url: String,
        val high_fee_per_kb: Long,
        val medium_fee_per_kb: Long,
        val low_fee_per_kb: Long,
        val unconfirmed_count: Int,
        val last_fork_height: Int,
        val last_fork_hash: String
)

