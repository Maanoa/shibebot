package de.langerhans.discord.doge.giphy

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by maxke on 31.03.2016.
 * API service to get data from Cryptonator
 */

interface GiphyService {

    @GET("search?&api_key=dc6zaTOxFJmzC&limit=25")
    fun searchGif(@Query("q") q: String, @Query("rating") rating: String): Call<Giphy>

    companion object {
        fun create(): GiphyService {
            val retrofit = Retrofit.Builder()
                    .baseUrl("http://api.giphy.com/v1/gifs/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit.create(GiphyService::class.java)
        }
    }
}