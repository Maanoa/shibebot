package de.langerhans.discord.doge.giphy

/**
 * Created by maxke on 02.04.2016.
 * Giphy API model, simplified for our needs
 */

data class Giphy (
        val data: List<GiphyData>,
        val meta: GiphyMeta
)

data class GiphyData (
        val images: GiphyImages
)

data class GiphyMeta (
        val status: Int,
        val msg: String
)

data class GiphyImages (
        val fixed_height: GiphyImage
)

data class GiphyImage (
        val url: String
)