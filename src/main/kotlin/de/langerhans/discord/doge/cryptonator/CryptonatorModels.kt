package de.langerhans.discord.doge.cryptonator

/**
 * Created by maxke on 31.03.2016.
 * JSON models
 */

data class Currencies (
        val rows: List<Currency>
)

data class Currency (
        val code: String,
        val name: String,
        val statuses: List<String>
)

data class BaseTicker (
        val ticker: Ticker,
        val timestamp: Long,
        val success: String,
        val error: String
)

data class Ticker (
        val base: String,
        val target: String,
        val price: String,
        val volume: String,
        val change: String,
        val markets: List<Market>
)

data class Market (
        val market: String,
        val price: String,
        val volume: String
)