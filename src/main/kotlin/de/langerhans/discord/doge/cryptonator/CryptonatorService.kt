package de.langerhans.discord.doge.cryptonator

import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import java.security.KeyStore
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManagerFactory

/**
 * Created by maxke on 31.03.2016.
 * API service to get data from Cryptonator
 */

interface CryptonatorService {

    @GET("currencies")
    fun currencies(): Call<Currencies>

    @GET("ticker/{base}-{target}")
    fun singleTicker(@Path("base") base: String, @Path("target") target: String): Call<BaseTicker>

    @GET("full/{base}-{target}")
    fun fullTicker(@Path("base") base: String, @Path("target") target: String): Call<BaseTicker>

    companion object {
        fun create(): CryptonatorService {
            val retrofit = Retrofit.Builder()
                    .client(createClient())
                    .baseUrl("https://www.cryptonator.com/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit.create(CryptonatorService::class.java)
        }

        fun createClient(): OkHttpClient {
            val ks = KeyStore.getInstance("JKS")
            val readStream = CryptonatorService::class.java.getResourceAsStream("/cryptonator.jks")
            ks.load(readStream, "cryptonator".toCharArray());
            readStream.close();
            val tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(ks);
            val ctx = SSLContext.getInstance("TLS");
            ctx.init(null, tmf.trustManagers, null);
            val sslFactory = ctx.socketFactory;

            return OkHttpClient.Builder().sslSocketFactory(sslFactory).build()
        }
    }
}