package de.langerhans.discord.doge

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import sx.blah.discord.api.IDiscordClient
import sx.blah.discord.handle.impl.events.MessageReceivedEvent
import sx.blah.discord.handle.obj.IChannel
import sx.blah.discord.handle.obj.Permissions
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

/**
 * Created by maxke on 04.04.2016.
 * Some useful extensions for Discord4J
 */

/**
 * Get a channel on this client by its name
 *
 * @param name Channel name
 * @param includePrivate Include private channels in the search
 * @return The channel or null if it doesn't exist
 */
fun IDiscordClient.getChannelByName(name: String, includePrivate: Boolean): IChannel? {
    val filtered = getChannels(includePrivate).filter { it.name == name }
    if (filtered.size == 1) {
        return filtered.first()
    }
    return null
}

/**
 * Reply to a received message. Basically just sends a message to the channel
 * that triggered this event.
 *
 * @param text The text you want to reply with
 */
fun MessageReceivedEvent.reply(text: String) {
    message.channel.sendMessage(text)
}

fun MessageReceivedEvent.authorCanManageServer(): Boolean {
    if (message.guild.ownerID == message.author.id) {
        return true // User is creator of guild, so implicitly has MANAGE_SERVER perms
    }
    val roles = message.author.getRolesForGuild(message.guild)
    var canManageServer = false
    roles.forEach { r -> if(r.permissions.contains(Permissions.MANAGE_SERVER)) canManageServer = true}
    if (!canManageServer) {
        val log: Logger = LoggerFactory.getLogger("AdminChecker")
        log.info("User ${message.author.name} tried to use an admin only command!")
    }
    return canManageServer
}

fun MessageReceivedEvent.authorIsBotAdmin(): Boolean {
    val admin = Injekt.get<Config>().config.adminId
    return message.author.id == admin
}