package de.langerhans.discord.doge.db.dao

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import de.langerhans.discord.doge.commands.SeenWatcher

/**
 * Created by maxke on 09.04.2016.
 * User DAO
 */

@DatabaseTable()
class Guild() {

    @DatabaseField(id = true)
    var guildId: String = ""

    /**
     * CSV of command names that are enabled
     */
    @DatabaseField(defaultValue = "config,help")
    var enabledCommands: String = "config"

    constructor(id: String) : this() {
        guildId = id
    }
}