package de.langerhans.discord.doge.db.dao

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import de.langerhans.discord.doge.commands.SeenWatcher

/**
 * Created by maxke on 09.04.2016.
 * User DAO
 */

@DatabaseTable()
class User() {

    @DatabaseField(id = true)
    var userId: String = ""

    @DatabaseField
    var lastSeenTime: Long = 0

    @DatabaseField
    var lastSeenType: SeenWatcher.SeenType = SeenWatcher.SeenType.UNKNOWN

    @DatabaseField
    var lastSeenGuildId: String = ""

    constructor(id: String) : this() {
        userId = id
    }
}