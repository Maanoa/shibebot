package de.langerhans.discord.doge.db.dao

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable

/**
 * Created by maxke on 09.04.2016.
 * User DAO
 */

@DatabaseTable()
class BotConfig() {

    @DatabaseField(id = true, defaultValue = "0")
    var id: Int = 0;

    @DatabaseField(defaultValue = "1")
    var dbVersion: Int = 1

}