package de.langerhans.discord.doge.db.dao

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable

/**
 * Created by maxke on 09.04.2016.
 * User DAO
 */

@DatabaseTable()
class GifConfig() {

    @DatabaseField(id = true, defaultValue = "0")
    var guildId: String = "";

    @DatabaseField(defaultValue = "pg-13")
    var rating: String = "pg-13"

    constructor(guildId: String): this() {
        this.guildId = guildId
    }

}