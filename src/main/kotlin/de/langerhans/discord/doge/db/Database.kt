package de.langerhans.discord.doge.db

import com.j256.ormlite.dao.Dao
import com.j256.ormlite.dao.DaoManager
import com.j256.ormlite.jdbc.JdbcConnectionSource
import com.j256.ormlite.table.TableUtils
import de.langerhans.discord.doge.db.dao.*
import org.slf4j.Logger
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.fullType
import uy.kohesive.injekt.api.get
import uy.kohesive.injekt.injectLogger

/**
 * Created by maxke on 08.04.2016.
 * DB access methods
 */
class Database(val h2dir: String) {

    private val log: Logger by injectLogger()

    private val VERSION_LAUNCH = 1

    private val DATABASE_VERSION = VERSION_LAUNCH

    val source: JdbcConnectionSource

    init {
        source = JdbcConnectionSource("jdbc:h2:file:$h2dir;AUTO_SERVER=TRUE")

        TableUtils.createTableIfNotExists(source, User::class.java)
        TableUtils.createTableIfNotExists(source, Guild::class.java)
        TableUtils.createTableIfNotExists(source, BotConfig::class.java)
        TableUtils.createTableIfNotExists(source, GifConfig::class.java)

        // Check for DB upgrade
        val configDao: Dao<BotConfig, Int> = DaoManager.createDao(source, BotConfig::class.java)
        var currentVersion: Int;
        val currentConfig = configDao.queryForId(0)

        if (currentConfig == null) {
            configDao.create(BotConfig());
            currentVersion = VERSION_LAUNCH
        } else {
            currentVersion = currentConfig.dbVersion
        }

        if (currentVersion < DATABASE_VERSION) {
            log.info("DB needs upgrade. Local DB version is $currentVersion, most current version is $DATABASE_VERSION")
            onUpgrade(currentVersion, DATABASE_VERSION)
        }
    }

    private fun onUpgrade(oldVersion: Int, newVersion: Int) {
        /* Not needed yet
        var version = oldVersion

        when(version) {
            VERSION_LAUNCH -> {addColumn(COL_X); version = VERSION_X}
            VERSION_X -> {addColumn(COL_FOO); version = VERSION_FOO}
        }

        if (version != newVersion) {
            onUpgrade(version, newVersion)
        } */
    }

    class DaoInjektModule(val source: JdbcConnectionSource): InjektModule {
        override fun InjektRegistrar.registerInjectables() {
            addSingletonFactory(fullType<Dao<User, String>>(), {DaoManager.createDao(source, User::class.java)})
            addSingletonFactory(fullType<Dao<Guild, String>>(), {DaoManager.createDao(source, Guild::class.java)})
            addSingletonFactory(fullType<Dao<BotConfig, Int>>(), {DaoManager.createDao(source, BotConfig::class.java)})

            // Commands
            addSingletonFactory(fullType<Dao<GifConfig, String>>(), {DaoManager.createDao(source, GifConfig::class.java)})
        }
    }
}