package de.langerhans.discord.doge

import de.langerhans.discord.doge.db.Database
import de.langerhans.discord.doge.web.BotConfigApplication
import de.langerhans.discord.doge.web.ConfigController
import de.langerhans.discord.doge.web.IndexController
import de.langerhans.discord.doge.web.WebAccessStorage
import org.slf4j.LoggerFactory
import ro.pippo.core.Pippo
import sx.blah.discord.api.ClientBuilder
import sx.blah.discord.api.IDiscordClient
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.InjektMain
import uy.kohesive.injekt.api.*

/**
 * Created by maxke on 31.03.2016.
 * Main entry
 */
open class Application {
    companion object {
        lateinit var configPath: String
        @JvmStatic fun main(args: Array<String>) {
            if (args.size < 1) {
                println("No config file specified! Pass a config.json as first parameter to the application. Exiting...")
                return
            }

            configPath = args[0]
            Application().run()
        }
    }

    fun run() {
        Injekt()
        Injekt.get<IDiscordClient>()
        Pippo(BotConfigApplication()).start()
    }
}

class Injekt(): InjektMain() {
    override fun InjektRegistrar.registerInjectables() {
        addLoggerFactory({ byName -> LoggerFactory.getLogger(byName) }, { byClass -> LoggerFactory.getLogger(byClass) })
        addSingleton(Config(Application.configPath))
        addSingleton(Database(Injekt.get<Config>().config.h2dir))
        importModule(Database.DaoInjektModule(Injekt.get<Database>().source))
        addSingleton(EventHandler())
        addSingletonFactory(fullType<IDiscordClient>(), {
            val config: Config = Injekt.get()
            val client = ClientBuilder().withToken(config.config.discordtoken).login()
            client.dispatcher.registerListener(Injekt.get<EventHandler>())
            return@addSingletonFactory client
        })
        addSingleton(WebAccessStorage())
    }
}