package de.langerhans.discord.doge

import com.google.gson.Gson
import java.io.File

/**
 * Created by maxke on 08.04.2016.
 * Main app config
 */

class Config(configPath: String) {
    lateinit var config: BotConfig
        get

    init {
        val configFile = File(configPath).readText()
        config = Gson().fromJson(configFile, BotConfig::class.java)
    }
}

data class BotConfig (
        val discordtoken: String,
        val h2dir: String,
        val trigger: String,
        val baseurl: String,
        val adminId: String
)