package de.langerhans.discord.doge.reddit

import com.google.gson.Gson
import de.langerhans.discord.doge.getChannelByName
import okhttp3.OkHttpClient
import okhttp3.Request
import org.slf4j.Logger
import sx.blah.discord.api.IDiscordClient
import uy.kohesive.injekt.injectLazy
import uy.kohesive.injekt.injectLogger
import java.util.concurrent.TimeUnit
import kotlin.concurrent.fixedRateTimer

/**
 * Created by maxke on 03.04.2016.
 * Watches Reddit for new posts
 */

class RedditWatcher {
    private var lastPosted = 0.toLong()
    private val httpClient = OkHttpClient()
    private val log: Logger by injectLogger()
    private val client: IDiscordClient by injectLazy()

    fun start() {
        fixedRateTimer("RedditWatcher", false, 0, TimeUnit.SECONDS.toMillis(60), {
            checkNew()
        })
    }

    private fun checkNew() {
        val req = Request.Builder()
            .url("https://www.reddit.com/r/dogecoin/new.json?limit=5")
            .build()

        val res = httpClient.newCall(req).execute()
        if (res.isSuccessful) {
            val listing = Gson().fromJson(res.body().string(), Reddit::class.java)

            var posts = listing.data.children.sortedBy { it.data.created_utc }
            if (lastPosted == 0.toLong()) {
                // If we never posted, we will nothing
                posts = posts.takeLast(1)
                lastPosted = posts[0].data.created_utc
                return
            } else {
                posts = posts.filter { post -> post.data.created_utc > lastPosted }
            }

            posts.forEach { post -> run {
                val s = StringBuilder()
                val data = post.data
                if (data.is_self) s.append("*Self post:*") else s.append("*Link post:*")
                s.append(" **\"${data.title}\"** posted in /r/${data.subreddit} by ${data.author}. https://redd.it/${data.name.drop(3)}")
                if (data.over_18) s.append(" (NSFW)")

                post(s.toString())

                if (data.created_utc > lastPosted) lastPosted = data.created_utc
            } }
        }
    }

    private fun post(s: String) {
        if (!client.isReady) {
            log.info("Bot was not ready when trying to post Reddit thread!")
            return
        }

        val channel = client.getChannelByName("reddit", false)
        channel?.sendMessage(s)
    }
}