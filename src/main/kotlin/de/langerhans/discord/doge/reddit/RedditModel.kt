package de.langerhans.discord.doge.reddit

/**
 * Created by maxke on 03.04.2016.
 * Simplified model for the Reddit listings
 */

data class Reddit (
        val kind: String,
        val data: RedditData
)

data class RedditData (
        val modhash: String,
        val before: String,
        val after: String,
        val children: List<Children>
)

data class Children (
        val kind: String,
        val data: ChildrenData
)

data class ChildrenData (
        val subreddit: String,
        val is_self: Boolean,
        val name: String,
        val author: String,
        val created_utc: Long,
        val title: String,
        val over_18: Boolean
)