## ShibeBot
A bot for Discord, written in Kotlin.

#### How to build
```bash
./gradlew build
```
You will find the application jar in `build/libs/`.

#### How to configure
Copy `config.sample.json` to `config.json` and edit it to your needs.

#### How to run
```bash
java -jar shibebot.jar /path/to/config.json
```